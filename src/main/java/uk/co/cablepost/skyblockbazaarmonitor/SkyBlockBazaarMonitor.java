package uk.co.cablepost.skyblockbazaarmonitor;

import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import uk.co.cablepost.skyblockbazaarmonitor.commands.*;

import java.util.Random;

@Mod(modid = "skyblockbazaarmonitor", version = "1.0.1")
public class SkyBlockBazaarMonitor {

    // "https://api.cablepost.co.uk/SkyBlockBazaarMonitor/logBazaarMessage"
    // "http://localhost:3000/SkyBlockBazaarMonitor"
    //public static String monitor_api = "";
    public static String player_uuid_string = "";//Set on first use

    public static String last_buy_hover_item_stack_item_name = "";
    //public static String last_buy_hover_item_stack_total_value = "";
    //public static String last_buy_hover_item_stack_item_count = "";
    public static String last_buy_hover_item_stack_price_per_unit = "";

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        SkyBlockBazaarMonitorConfig.preInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(new SkyBlockBazaarMonitorEventHandler());

        //ClientCommandHandler.instance.registerCommand(new SkyBlockBazaarMonitorSetEndpointCommand());

        //ClientCommandHandler.instance.registerCommand(new SkyBlockBazaarMonitorNewPasswordCommand());
        //ClientCommandHandler.instance.registerCommand(new SkyBlockBazaarMonitorGetPasswordCommand());
        //ClientCommandHandler.instance.registerCommand(new SkyBlockBazaarMonitorSetPasswordCommand());

        ClientCommandHandler.instance.registerCommand(new SkyBlockBazaarMonitorOpenConfigCommand());
        ClientCommandHandler.instance.registerCommand(new SkyBlockBazaarMonitorReloadConfigCommand());
    }

    public static String generateNewSecret(){
        //byte[] array = new byte[32];
        //new Random().nextBytes(array);
        //return new String(array, StandardCharsets.UTF_8);

        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder toRet = new StringBuilder();
        for(int i = 0; i < 32; i++){
            toRet.append(chars.charAt(new Random().nextInt(chars.length())));
        }

        return toRet.toString();
    }
}
