package uk.co.cablepost.skyblockbazaarmonitor.mixin;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitor;

import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.core.helpers.Integers.parseInt;

@Mixin(ItemStack.class)
public abstract class ItemStackMixin {
    @Shadow public abstract String toString();

    @Inject(method = "getTooltip", at = @At("RETURN"))
    public void getTooltip(EntityPlayer player, boolean advanced, CallbackInfoReturnable<List<String>> cir) {

        ItemStack itemStack = ((ItemStack)(Object)this);

        if(player == null){
            return;
        }

        if(itemStack.getItem() == null){
            return;
        }

        List<String> lines = new ArrayList<>(cir.getReturnValue());

        //System.out.println("---");
        lines.replaceAll(s -> {
            return s.replaceAll("§\\w", "");
            //System.out.println(i + ": " + lines.get(i));
        });

        if(!lines.get(0).startsWith("BUY")){
            return;
        }

        if(!lines.get(4).endsWith("100%!")){
            return;
        }

        SkyBlockBazaarMonitor.last_buy_hover_item_stack_item_name = lines.get(0).replace("BUY ", "");
        //SkyBlockBazaarMonitor.last_buy_hover_item_stack_total_value = shortNumToFull(lines.get(1).replace("Worth ", "").replace(" coins", ""));
        //SkyBlockBazaarMonitor.last_buy_hover_item_stack_item_count = shortNumToFull(lines.get(3).replace("Order amount: ", "").replace("x", ""));
        SkyBlockBazaarMonitor.last_buy_hover_item_stack_price_per_unit = shortNumToFull(lines.get(6).replace("Price per unit: ", "").replace(" coins", ""));

        //SkyBlockBazaarMonitor.last_buy_hover_item_stack_total_value = String.valueOf(Double.parseDouble(SkyBlockBazaarMonitor.last_buy_hover_item_stack_price_per_unit) * parseInt(SkyBlockBazaarMonitor.last_buy_hover_item_stack_item_count));
    }

    private String shortNumToFull(String num){
        return num
                .replace("k", "000")
                .replace("m", "000000")
                .replace("b", "000000000")
                .replace("t", "000000000000");
    }
}
