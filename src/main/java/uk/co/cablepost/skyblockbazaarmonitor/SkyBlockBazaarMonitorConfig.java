package uk.co.cablepost.skyblockbazaarmonitor;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.io.File;

public class SkyBlockBazaarMonitorConfig {
    private static Configuration config = null;

    public static String password;
    public static String endpoint;

    public static void preInit(){
        File configFile = new File(Loader.instance().getConfigDir(), "SkyBlockBazaarMonitor.cfg");
        config = new Configuration(configFile);

        syncFromFiles();
    }

    public static Configuration getConfig(){
        return config;
    }

    public static void clientPreInit() {
        MinecraftForge.EVENT_BUS.register(new ConfigEventHandler());
    }

    public static void syncFromFiles(){
        syncConfig(true, true);
    }

    public static void syncFromGui(){
        syncConfig(false, true);
    }

    public static void syncFromFields(){
        syncConfig(false, false);
    }

    public static void syncConfig(boolean loadFromConfigFile, boolean readFieldsFromConfig){
        if(loadFromConfigFile){
            config.load();
        }

        Property propertyEndpoint = config.get("main", "endpoint", "https://example.com/test");
        propertyEndpoint.comment = "The endpoint you call each time you want to log something (see https://www.curseforge.com/minecraft/mc-mods/hypixel-skyblock-bazaar-monitor for info).";

        Property propertyPassword = config.get("main", "password", SkyBlockBazaarMonitor.generateNewSecret());
        propertyPassword.comment = "A password can be set in the Google App Script to make sure requests only come from people you want, if this is enabled you can put the password here.";

        if(readFieldsFromConfig) {
            endpoint = propertyEndpoint.getString();
            password = propertyPassword.getString();
        }

        propertyEndpoint.set(endpoint);
        propertyPassword.set(password);

        if(config.hasChanged()){
            config.save();
        }
    }

    public static class ConfigEventHandler {
        @SubscribeEvent(priority = EventPriority.LOWEST)
        public void onEvent(ConfigChangedEvent.OnConfigChangedEvent event) {
            if(event.modID.equals("skyblockbazaarmonitor")) {
                syncFromGui();
            }
        }
    }
}
