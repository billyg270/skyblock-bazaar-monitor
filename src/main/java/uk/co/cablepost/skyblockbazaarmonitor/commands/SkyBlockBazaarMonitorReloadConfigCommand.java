package uk.co.cablepost.skyblockbazaarmonitor.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitorConfig;

public class SkyBlockBazaarMonitorReloadConfigCommand extends CommandBase {
    @Override
    public String getCommandName() {
        return "bazaarLoggerReloadConfig";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "Reloads the config from the config file /bazaarLoggerReloadConfig";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        if(args.length != 0){
            sender.addChatMessage(new ChatComponentText("§5No parameters required"));
            return;
        }

        SkyBlockBazaarMonitorConfig.syncFromFiles();

        sender.addChatMessage(new ChatComponentText("§5Config reloaded"));
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }
}
