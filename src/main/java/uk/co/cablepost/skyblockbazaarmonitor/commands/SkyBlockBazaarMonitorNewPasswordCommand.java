package uk.co.cablepost.skyblockbazaarmonitor.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitor;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitorConfig;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

public class SkyBlockBazaarMonitorNewPasswordCommand extends CommandBase {
    @Override
    public String getCommandName() {
        return "bazaarLoggerNewPassword";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "Generate a new password for Bazaar Logger - copies to clipboard";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        SkyBlockBazaarMonitorConfig.password = SkyBlockBazaarMonitor.generateNewSecret();
        SkyBlockBazaarMonitorConfig.syncFromFields();

        Toolkit.getDefaultToolkit()
                .getSystemClipboard()
                .setContents(
                        new StringSelection(SkyBlockBazaarMonitorConfig.password),
                        null
                );

        //ChatStyle passwordStyle = new ChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://cablepost.co.uk/mc/skyblockbazaarmonitor?password=" + SkyBlockBazaarMonitor.secret_string));
        sender.addChatMessage(new ChatComponentText("§5New Bazaar Logger Password generated and copied to clipboard"));
        //sender.addChatMessage(new ChatComponentText("§6" + SkyBlockBazaarLogger.secret_string).setChatStyle(passwordStyle));
        sender.addChatMessage(new ChatComponentText("§5Do not share this password with anyone unless they are in your co-op!"));
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }
}
