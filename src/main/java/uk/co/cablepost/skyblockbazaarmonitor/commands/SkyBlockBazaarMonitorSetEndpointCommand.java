package uk.co.cablepost.skyblockbazaarmonitor.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitorConfig;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

public class SkyBlockBazaarMonitorSetEndpointCommand extends CommandBase {
    @Override
    public String getCommandName() {
        return "bazaarLoggerSetEndpoint";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "Set a new endpoint for Bazaar Logger from your clipboard /bazaarLoggerSetEndpoint";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        if(args.length != 0){
            sender.addChatMessage(new ChatComponentText("§5No parameters required, copy the URL to your clipboard then run this command"));
            return;
        }

        Toolkit.getDefaultToolkit()
                .getSystemClipboard()
                .getContents(
                        new StringSelection(SkyBlockBazaarMonitorConfig.endpoint)
                );

        SkyBlockBazaarMonitorConfig.syncFromFields();

        sender.addChatMessage(new ChatComponentText("§5Bazaar Logger Endpoint set"));
        sender.addChatMessage(new ChatComponentText("§5Do not share this endpoint with anyone unless they are in your co-op!"));
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }
}
