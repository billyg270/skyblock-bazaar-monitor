package uk.co.cablepost.skyblockbazaarmonitor.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitor;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitorConfig;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

public class SkyBlockBazaarMonitorSetPasswordCommand extends CommandBase {
    @Override
    public String getCommandName() {
        return "bazaarLoggerSetPassword";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "Set a new password for Bazaar Logger /bazaarLoggerSetPassword <password>";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        if(args.length != 1){
            sender.addChatMessage(new ChatComponentText("§5Requires one parameter /bazaarLoggerSetPassword <password>"));
            return;
        }

        SkyBlockBazaarMonitorConfig.password = args[0];
        SkyBlockBazaarMonitorConfig.syncFromFields();

        sender.addChatMessage(new ChatComponentText("§5New Bazaar Logger Password set"));
        sender.addChatMessage(new ChatComponentText("§5Do not share this password with anyone unless they are in your co-op!"));
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }
}
