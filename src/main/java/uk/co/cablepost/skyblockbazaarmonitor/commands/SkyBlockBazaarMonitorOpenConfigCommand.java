package uk.co.cablepost.skyblockbazaarmonitor.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.fml.common.Loader;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitorConfig;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class SkyBlockBazaarMonitorOpenConfigCommand extends CommandBase {
    @Override
    public String getCommandName() {
        return "bazaarLoggerOpenConfig";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "Opens the config file in your default text editor /bazaarLoggerOpenConfig";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        if(args.length != 0){
            sender.addChatMessage(new ChatComponentText("§5No parameters required"));
            return;
        }

        try {
            Desktop.getDesktop().open(new File(Loader.instance().getConfigDir(), "SkyBlockBazaarMonitor.cfg"));
            sender.addChatMessage(new ChatComponentText("§5Application opened"));
        } catch (IOException e) {
            sender.addChatMessage(new ChatComponentText("§5Could not open: " + e.getMessage()));
        }
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }
}
