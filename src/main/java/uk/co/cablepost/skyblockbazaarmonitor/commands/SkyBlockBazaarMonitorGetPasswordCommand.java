package uk.co.cablepost.skyblockbazaarmonitor.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitor;
import uk.co.cablepost.skyblockbazaarmonitor.SkyBlockBazaarMonitorConfig;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

public class SkyBlockBazaarMonitorGetPasswordCommand extends CommandBase {
    @Override
    public String getCommandName() {
        return "bazaarLoggerGetPassword";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "Get password for Bazaar Logger - copies to clipboard";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException {
        Toolkit.getDefaultToolkit()
                .getSystemClipboard()
                .setContents(
                        new StringSelection(SkyBlockBazaarMonitorConfig.password),
                        null
                );

        sender.addChatMessage(new ChatComponentText("§5Bazaar Logger Password copied to clipboard"));
        sender.addChatMessage(new ChatComponentText("§5Do not share this password with anyone unless they are in your co-op!"));
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }
}
