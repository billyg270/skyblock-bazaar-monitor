package uk.co.cablepost.skyblockbazaarmonitor;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.logging.log4j.core.helpers.Integers.parseInt;

public class SkyBlockBazaarMonitorEventHandler {
    @SubscribeEvent(priority = EventPriority.HIGH)
    public void onChatMessage(ClientChatReceivedEvent event){
        IChatComponent iChatComponent = event.message;
        String chatMessage = iChatComponent.getUnformattedText();

        if(chatMessage.startsWith("[Bazaar] ")) {
            Matcher sellClaimMatcher = Pattern.compile("Claimed (.*) coins from selling (.*)x (.*) at (.*) each!").matcher(chatMessage);

            if (sellClaimMatcher.find()) {
                sendToApi(
                        sellClaimMatcher.group(3),//itemName
                        "sell",
                        "bazaar",
                        sellClaimMatcher.group(2).replaceAll(",", ""),//itemAmount
                        sellClaimMatcher.group(1).replaceAll(",", ""),//coinAmount
                        sellClaimMatcher.group(4).replaceAll(",", "")//unitPrice
                );
            }

            Matcher buyClaimMatcher = Pattern.compile("Claimed (.*)x (.*) worth (.*) coins bought for (.*) each!").matcher(chatMessage);

            if (buyClaimMatcher.find()) {
                sendToApi(
                        buyClaimMatcher.group(2),//itemName
                        "buy",
                        "bazaar",
                        buyClaimMatcher.group(1).replaceAll(",", ""),//itemAmount
                        buyClaimMatcher.group(3).replaceAll(",", ""),//coinAmount
                        buyClaimMatcher.group(4).replaceAll(",", "")//unitPrice
                );
            }

            Matcher instaSellMatcher = Pattern.compile("Sold (.*)x (.*) for (.*) coins!").matcher(chatMessage);

            if (instaSellMatcher.find()) {
                sendToApi(
                        instaSellMatcher.group(2),//itemName
                        "sell",
                        "bazaar",
                        instaSellMatcher.group(1).replaceAll(",", ""),//itemAmount
                        instaSellMatcher.group(3).replaceAll(",", ""),//coinAmount
                        String.valueOf(Double.parseDouble(instaSellMatcher.group(3).replaceAll(",", "")) / parseInt(instaSellMatcher.group(1).replaceAll(",", "")))//unitPrice
                );
            }

            Matcher instaBuyMatcher = Pattern.compile("Bought (.*)x (.*) for (.*) coins!").matcher(chatMessage);

            if (instaBuyMatcher.find()) {
                sendToApi(
                        instaBuyMatcher.group(2),//itemName
                        "buy",
                        "bazaar",
                        instaBuyMatcher.group(1).replaceAll(",", ""),//itemAmount
                        instaBuyMatcher.group(3).replaceAll(",", ""),//coinAmount
                        String.valueOf(Double.parseDouble(instaBuyMatcher.group(3).replaceAll(",", "")) / parseInt(instaBuyMatcher.group(1).replaceAll(",", "")))//unitPrice
                );
            }

            Matcher flipMatcher = Pattern.compile("Order Flipped! (.*)x (.*) for (.*) coins of total expected profit.").matcher(chatMessage);

            if (flipMatcher.find()) {
                sendToApi(
                        SkyBlockBazaarMonitor.last_buy_hover_item_stack_item_name,
                        "buy",
                        "bazaar",
                        flipMatcher.group(1).replaceAll(",", ""),//itemAmount
                        String.valueOf(Double.parseDouble(SkyBlockBazaarMonitor.last_buy_hover_item_stack_price_per_unit.replaceAll(",", "")) * parseInt(flipMatcher.group(1).replaceAll(",", ""))),//coinAmount
                        SkyBlockBazaarMonitor.last_buy_hover_item_stack_price_per_unit.replaceAll(",", "")
                );
            }
        }
    }

    private void sendToApi(
            String itemName,
            String action,
            String store,
            String itemAmount,
            String coinAmount,
            String unitPrice
    ){
        Minecraft minecraftClient = Minecraft.getMinecraft();

        if(Objects.equals(SkyBlockBazaarMonitor.player_uuid_string, "")){
            //Has not been set yet, fetch now
            SkyBlockBazaarMonitor.player_uuid_string = minecraftClient.getSession().getPlayerID();//Gets the logged in player's UUID
        }

        HttpClient httpClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();
        try {
            String paramsStr = "{"
                    .concat("\"playerUUID\":\"" + SkyBlockBazaarMonitor.player_uuid_string + "\",")
                    .concat("\"secret\":\"" + SkyBlockBazaarMonitorConfig.password + "\",")
                    .concat("\"itemName\":\"" + itemName + "\",")
                    .concat("\"action\":\"" + action + "\",")
                    .concat("\"store\":\"" + store + "\",")
                    .concat("\"itemAmount\":" + itemAmount.replaceAll(",", "") + ",")
                    .concat("\"coinAmount\":" + coinAmount.replaceAll(",", "") + ",")
                    .concat("\"unitPrice\":" + unitPrice.replaceAll(",", "") + "")
                    .concat("}");
            System.out.println(paramsStr);
            HttpPost request = new HttpPost(SkyBlockBazaarMonitorConfig.endpoint);
            StringEntity params = new StringEntity(paramsStr);
            request.addHeader("content-type", "application/json");
            request.setEntity(params);

            new Thread(() -> {
                HttpResponse response = null;
                try {
                    response = httpClient.execute(request);

                    if(response.getStatusLine().getStatusCode() != 200){
                        minecraftClient.ingameGUI.getChatGUI().printChatMessage(new ChatComponentText("Could not log transaction. See logs for details"));
                        LogManager.getLogger().log(Level.ERROR, "Status code from API: " + response.getStatusLine().getStatusCode());
                        LogManager.getLogger().log(Level.ERROR, "Message from API: " + EntityUtils.toString(response.getEntity(), "UTF-8"));
                    }
                } catch (IOException ex) {
                    minecraftClient.ingameGUI.getChatGUI().printChatMessage(new ChatComponentText("Could not log transaction message. See logs for details"));
                    LogManager.getLogger().log(Level.ERROR, "Error sending message to API:" + ex.getMessage());
                }
            }).start();
        } catch (Exception ex) {
            minecraftClient.ingameGUI.getChatGUI().printChatMessage(new ChatComponentText("Could not log transaction message. See logs for details"));
            LogManager.getLogger().log(Level.ERROR, "Error sending message to API:" + ex.getMessage());
        }
    }
}
