// By BillBodkin. Feel free to modify to fit your needs.

const doPost = (request) => {
  const jsonData = JSON.parse(request.postData.contents);
  // To use a password, remove the "//" from the next 3 lines of code and replace "TYPE PASSWORD HERE" with what you want (But keep the quotes around the password!)
  //if(jsonData.secret != "TYPE PASSWORD HERE"){
  //  throw Error("Incorrect password");
  //}

  var sheet = SpreadsheetApp.getActiveSheet();
  var data = sheet.getDataRange().getValues();

  if(data.length == 1 && data[0][0].trim() == ""){
    // New sheet, add header row
    sheet.appendRow([
      "UTC Timestamp (yyyy-mm-dd)",
      "Player",
      "Item Name",
      "Action",
      "Store",
      "Item Amount",
      "Coin Amount",
      "Unit Price"
    ]);
  }

  sheet.appendRow([
    Utilities.formatDate(new Date(), "UTC", "yyyy-MM-dd HH:mm:ss"),
    jsonData.playerUUID,
    jsonData.itemName,
    jsonData.action,
    jsonData.store,
    jsonData.itemAmount,
    jsonData.coinAmount,
    jsonData.unitPrice
  ]);

  //return ContentService.createTextOutput("Added");
  return ContentService.createTextOutput(JSON.stringify(request));
};